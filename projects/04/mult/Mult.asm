// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
//
// This program only needs to handle arguments that satisfy
// R0 >= 0, R1 >= 0, and R0*R1 < 32768.

// Put your code here.
// RAM[2] = 0
@R2
M=0
// a = RAM[0]
@R0
D=M
@a
M=D
// b = RAM[1]
@R1
D=M
@b
M=D
// i = 0
@0
D=A
@i
M=D
// sum = 0
@0
D=A
@sum
M=D
(LOOP) 
// sum = sum + a
@sum
D=M
@a
D=D+M
@sum
M=D
// i = i + 1
@i
M=M+1
// if b - i == 0 ; GOTO LOOP
@b
D=M
@i
D=D-M
@LOOP
D;JNE
@sum
D=M
@R2
M=D
